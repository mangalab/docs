# MangaLab Documentation

The official MangaLab Documentation

## Getting Started

A central location on how to use MangaLab and all API details will be documented throughout the development process.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## License

This Documentation is currently licensed under the Apache 2.0 License - see the [LICENSE.md](LICENSE.md) file for details
